#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

import random
import score

BID_REDOUBLE = -2
BID_DOUBLE = -1
BID_PASS = 0
BID_CLUBS = 1
BID_DIAMONDS = 2
BID_HEARTS = 3
BID_SPADES = 4
BID_NO_TRUMPS = 5
BID_ALL_TRUMPS = 6

BIDS = {BID_REDOUBLE, BID_DOUBLE, BID_PASS, BID_CLUBS, BID_DIAMONDS, BID_HEARTS, BID_SPADES, BID_NO_TRUMPS, BID_ALL_TRUMPS}

TRUMPS = {
	BID_CLUBS: [0],
	BID_DIAMONDS: [1],
	BID_HEARTS: [2],
	BID_SPADES: [3],
	BID_NO_TRUMPS: [],
	BID_ALL_TRUMPS: [0, 1, 2, 3]
}

class card():
	"""Represents a card"""

	_suits = ("c", "d", "h", "s")
	_ranks = ("7", "8", "9", "10", "J", "Q", "K", "A")

	def __init__(self, suit, rank=None):
		"""Creates a card of given suit and rank"""

		# Handle initialization from string (e.g. "10h")
		if rank == None:
			rank = suit[:-1]
			suit = suit[-1]

		if isinstance(suit, int):
			self.suit = suit
		else:
			translate = {value: index for index, value in enumerate(self._suits)}
			self.suit = translate[suit]

		if isinstance(rank, int):
			self.rank = rank
		else:
			translate = {value: index for index, value in enumerate(self._ranks)}
			self.rank = translate[rank]

	def __eq__(self, other):
		return ((self.suit == other.suit) and (self.rank == other.rank))

	def __hash__(self):
		return (self.suit * 8 + self.rank)

	def __repr__(self):
		return "card('" + self._ranks[self.rank] + self._suits[self.suit] + "')"

	def __str__(self):
		"""Returns the name of the picture of the card"""
		return self._ranks[self.rank] + self._suits[self.suit]

	def power(self, trumps):
		"""Returns the power of the card"""
		is_trump = self.suit in trumps
		return ((0, 1, 2, 6, 3, 4, 5, 7), (8, 9, 14, 12, 15, 10, 11, 13))[is_trump][self.rank]

	def score(self, trumps):
		"""Returns the points that the card is worth"""
		is_trump = self.suit in trumps
		return ((0, 0, 0, 10, 2, 3, 4, 11), (0, 0, 14, 10, 20, 3, 4, 11))[is_trump][self.rank]

	def score_carre(self):
		return (0, 0, 150, 100, 200, 100, 100, 100)[self.rank]

def arrange(cards, trumps):
	"""Arranges the cards by suit and rank"""
	def index(card):
		offset = [2, 3, 1, 0][card.suit] * 8
		return offset + card.power(trumps) % 8
	return sorted(cards, key=index)

def deck():
	"""Generate a deck"""
	return [card(suit, rank) for suit in range(4) for rank in range(8)]

def deal():
	cards = deck()
	i = len(cards) - 1
	while i:
		switch = random.randint(0, i)
		cards[switch], cards[i] = cards[i], cards[switch]
		i -= 1
	return cards

def bonus(cards):
	def sequence(cards):
		length = len(cards)
		if length not in [3, 4, 5]:
			return False
		for i in range(1, length):
			if cards[i].suit != cards[0].suit:
				print("sequence suit", cards[i].suit, cards[0].suit)
				return False
			if (cards[i].rank - cards[0].rank) != i:
				#print("sequence rank", cards[i].rank - cards[0].suit) != i)
				return False
		return True

	def carre(cards):
		if len(cards) != 4:
			return False
		if not cards[0].score_carre():
			return False
		return all([c.rank == cards[0].rank for c in cards])

	cards = tuple(sorted(cards, key=hash))

	if carre(cards):
		return (score.BONUS_CARRE, cards)
	elif sequence(cards):
		return (score.BONUS_SEQUENCE, cards)

	return None

def bonus_belote(deck, c, trumps):
	print(deck, c, trumps)
	if c.rank not in [5, 6]:
		return None
	if c.suit not in trumps:
		return None
	belote = (card(c.suit, 5), card(c.suit, 6))
	if len(set(deck) & set(belote)) == 2:
		return (score.BONUS_BELOTE, belote)
	return None

def hand_collect(hand, trumps):
	"""Determine which player collects the current hand"""

	def second(entry):
		return entry[1]

	suit = hand[0].suit

	if trumps and (suit not in trumps):
		hand_trumps = [(i, card.power(trumps)) for i, card in enumerate(hand) if card.suit in trumps]
		if hand_trumps:
			return max(hand_trumps, key=second)[0]

	hand_suit = [(i, card.power(trumps)) for i, card in enumerate(hand) if card.suit == suit]
	return max(hand_suit, key=second)[0]

def test_hand_collect():
	assert hand_collect([card("c", "9"), card("s", "8"), card("c", "A"), card("d", "J")], TRUMPS[BID_SPADES]) == 1
	assert hand_collect([card("c", "9"), card("s", "8"), card("c", "A"), card("d", "J")], TRUMPS[BID_NO_TRUMPS]) == 2
	assert hand_collect([card("c", "9"), card("s", "8"), card("c", "A"), card("d", "J")], TRUMPS[BID_CLUBS]) == 0
