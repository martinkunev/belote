A card game for 4 players. Supports networking. There is a rudimentary AI implemented for testing.

beta version

## Installation

Requires Python 3 with PIL and imagetk installed. On debian you can run `apt-get install python3-pil.imagetk`.

## Usage

Run with `python3 game.py` and enter player name. To create a game, select number of computers (between 0 and 3) and click Host. To join a game created by another player, enter IP address and click Join.

Left click to play a card.
Right click to play a card while announcing belote.
On first round, use the announce button and then right click to select tierce, quarte, quinte or carré.
