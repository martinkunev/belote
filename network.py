#! /usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

import socket
import json

import player
import belote

PORT = 4440

def host():
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.bind(("", PORT))
	sock.listen(4 - 1) # there is always 1 local player
	return sock

def accept(sock):
	return sock.accept()

def send(socket, event, **args):
	if event:
		args["event"] = event
	message = json.dumps(args).encode("ASCII")
	#print("sending ", message)
	packet = len(message).to_bytes(4, byteorder="big") + message
	socket.send(packet)

def recv(socket):
	size = int.from_bytes(socket.recv(4), byteorder="big")
	m = socket.recv(size).decode("ASCII")
	#print("received:", m)
	return json.loads(m)

def disconnect(sock):
	sock.close()

def join(server, name, ui):
	def makecards(cards):
		return [belote.card(c) if c != "None" else None for c in cards]

	def idle_playroom():
		table = player.rotate(local.cards, local.player)
		ui.playroom(table, local.hand, local.bonuses, player.rotate(local.play_hand, local.player))

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect((server, PORT))

	local = player.local(name, ui)
	info = None
	started = False
	while True:
		receive = None
		if started:
			receive = ui.asynchronous(recv, idle_playroom)
		else:
			receive = ui.asynchronous(recv, lambda: ui.waitroom(local.info))
		message = receive(sock)

		if message["event"] == "notify_deal":
			started = True
			local.notify_deal(message["player"], makecards(message["cards"]))
		elif message["event"] == "notify_bid":
			local.notify_bid(message["player"], message["bid"])
		elif message["event"] == "notify_play":
			bonuses = [(bonus[0], makecards(bonus[1])) for bonus in message["bonuses"]]
			local.notify_play(message["player"], belote.card(message["card"]), bonuses)
		elif message["event"] == "notify_score":
			hands = list(map(makecards, message["hands"]))
			local.notify_score(message["names"], message["context"], hands, message["result"], message["points"])
		elif message["event"] == "notify_restart":
			local.notify_restart()
		elif message["event"] == "notify_position":
			local.notify_position(message["position"], message["info"])
		elif message["event"] == "ask_join":
			if started:
				exit() # only valid if the game hasn't started
			name = local.ask_join(int(message["index"]), message["info"])
			ui.asynchronous(lambda sock: send(sock, "", name=name), lambda: ui.waitroom(message["info"]))(sock)
		elif message["event"] == "ask_bid":
			bid = local.ask_bid()
			ui.asynchronous(lambda sock: send(sock, "", bid=bid), idle_playroom)(sock)
		elif message["event"] == "ask_play":
			card, bonuses = local.ask_play()
			bonuses = [(bonus[0], list(map(str, bonus[1]))) for bonus in bonuses]
			ui.asynchronous(lambda sock: send(sock, "", card=str(card), bonuses=bonuses), idle_playroom)(sock)
