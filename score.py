#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

import belote

BONUS_BELOTE = 0
BONUS_SEQUENCE = 1
BONUS_CARRE = 2

SCORE_BELOTE = 20
SCORE_SEQUENCE = {3: 20, 4: 50, 5: 100}

POINTS_WIN = 151

class score():
	def __init__(self):
		self.points = [0, 0]
		self.hang = 0
		self.hanger = None

	def new(self):
		self.bid = 0
		self.bidder = None
		self.multiplier = 1

		self.belotes = [[], []]
		self.sequences = [[], []]
		self.carres = [[], []]
		self.hands = [[], []]

	def _rank_sequence(self, sequence):
		# Order sequences by length and then by highest card.
		offset = (len(sequence) - 3) * 8
		assert(offset >= 0)
		return offset + sequence[-1].rank

	def _rank_carre(self, sequence):
		# Order carres by card power when trump.
		assert len(sequence) == 4
		return sequence[0].power([sequence[0].suit])

	def bonuses_add(self, player, bonuses):
		team = player % 2
		for bonus in bonuses:
			if bonus[0] == BONUS_BELOTE:
				self.belotes[team].append(bonus[1])
			elif bonus[0] == BONUS_SEQUENCE:
				self.sequences[team].append(bonus[1])
			else:
				assert bonus[0] == BONUS_CARRE
				self.carres[team].append(bonus[1])

	def hand_add(self, player, hand):
		team = player % 2
		self.hands[team] += hand
		self.last = team

	def _calculate(self, index, valid_sequences, valid_carres, trumps):
		value = sum(card.score(trumps) for card in self.hands[index])
		if self.bid == belote.BID_NO_TRUMPS:
			# No bonuses to calculate. Double score.
			return value * 2

		value += SCORE_BELOTE * len(self.belotes[index])
		if valid_sequences[index]:
			value += sum(SCORE_SEQUENCE[len(bonus)] for bonus in self.sequences[index])
		if valid_carres[index]:
			value += sum(bonus[0].score_carre() for bonus in self.carres[index])
		return value

	def _winner(self, result, extra):
		total = [0, 0]
		total[0] = result[0] + extra[0]
		total[1] = result[1] + extra[1]

		if total[0] > total[1]:
			return 0
		elif total[1] > total[0]:
			return 1
		else:
			return -1

	def calculate(self):
		trumps = belote.TRUMPS[self.bid]

		top_sequences = [0, 0]
		top_sequences[0] = max(map(self._rank_sequence, self.sequences[0]), default=0)
		top_sequences[1] = max(map(self._rank_sequence, self.sequences[1]), default=0)

		top_carres = [0, 0]
		top_carres[0] = max(map(self._rank_carre, self.carres[0]), default=0)
		top_carres[1] = max(map(self._rank_carre, self.carres[1]), default=0)

		valid_sequences = (top_sequences[0] > top_sequences[1], top_sequences[1] > top_sequences[0])
		valid_carres = (top_carres[0] > top_carres[1], top_carres[1] > top_carres[0])

		count = [0, 0]
		count[0] = self._calculate(0, valid_sequences, valid_carres, trumps)
		count[1] = self._calculate(1, valid_sequences, valid_carres, trumps)
		count[self.last] += 20 if (self.bid == belote.BID_NO_TRUMPS) else 10

		# Add extra points for capot.
		extra = [90 * (len(self.hands[0]) == 32), 90 * (len(self.hands[1]) == 32)]

		# Deal with commitments and double.
		result = count.copy()
		winner = self._winner(result, extra)
		loser = 1 - winner
		raised = self.bidder % 2
		committed = [(self.multiplier > 1) or (raised == 0), (self.multiplier > 1) or raised]
		if winner >= 0:
			if committed[loser]:
				result[winner] = (result[0] + result[1]) * self.multiplier + extra[0] + extra[1]
				result[loser] = 0
			else:
				result[0] += extra[0]
				result[1] += extra[1]
			result[winner] += self.hang
			self.hang = 0
		else: # equality
			if self.multiplier > 1:
				self.hang += (result[0] + result[1]) * self.multiplier + extra[0] + extra[1]
				self.hanger = None
			elif committed[0]:
				if self.hanger == 0:
					result[1] += self.hang
					self.hang = 0
				self.hang += result[0] + extra[0]
				self.hanger = 0
				result[0] = 0
			else:
				assert committed[1]
				if self.hanger == 1:
					result[0] += self.hang
					self.hang = 0
				self.hang += result[1] + extra[1]
				self.hanger = 1
				result[1] = 0

		# Calculate game points.
		points = [0, 0]
		if self.bid == belote.BID_NO_TRUMPS:
			points[0] = round(result[0], -1) // 10
			points[1] = round(result[1], -1) // 10
		elif self.bid == belote.BID_ALL_TRUMPS:
			points[winner] = round(result[winner], -1) // 10
			points[loser] = round(result[loser] + 1, -1) // 10 # round up if last digit is 4
		else:
			points[winner] = round(result[winner] - 2, -1) // 10 # round down if last digit is 5 or 6
			points[loser] = round(result[loser] - 1, -1) // 10 # round down if last digit is 5

		self.points[0] += points[0]
		self.points[1] += points[1]

		return (self.points[0], count[0], valid_sequences[0], valid_carres[0], self.belotes[0]), (self.points[1], count[1], valid_sequences[1], valid_carres[1], self.belotes[1])

	def end(self):
		if self.points[0] == self.points[1]:
			return False
		if (self.points[0] >= POINTS_WIN) and (self.points[0] > self.points[1]):
			self.winner = 0
			return True
		if (self.points[1] >= POINTS_WIN) and (self.points[1] > self.points[0]):
			self.winner = 1
			return True
		return False

def test_alltrump():
	c = belote.card

	s = score()
	s.new()

	s.bidder = 1
	s.multiplier = 1

	s.belotes = [[], []]
	s.sequences = [[], []]
	s.carres = [[], []]
	s.hands = [[], []]

	s.hands[0] = [c("8c"), c("Ac"), c("9d"), c("Qc"), c("9c"), c("10s"), c("9h"), c("10c")]
	s.hands[1] = [c("7s"), c("Js"), c("Qs"), c("As"), c("7h"), c("10h"), c("Qh"), c("8h"), c("Jd"), c("Ad"), c("Kd"), c("10d"), c("Jh"), c("7d"), c("Kc"), c("Ah"), c("Kh"), c("Ks"), c("8d"), c("Jc"), c("Qd"), c("9s"), c("8s"), c("7c")]

	s.bid = belote.BID_ALL_TRUMPS
	result = s.calculate(0)
	assert result[0][1] == (11 + 14 + 3 + 14 + 10 + 14 + 10) + 10
	assert result[1][1] == (20 + 3 + 11 + 10 + 3 + 20 + 11 + 4 + 10 + 20 + 4 + 11 + 4 + 4 + 20 + 3 + 14)

	s.bid = belote.BID_NO_TRUMPS
	result = s.calculate(0)
	assert result[0][1] == (11 + 3 + 10 + 10) * 2 + 20
	assert result[1][1] == (2 + 3 + 11 + 10 + 3 + 2 + 11 + 4 + 10 + 2 + 4 + 11 + 4 + 4 + 2 + 3) * 2
