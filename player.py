#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

import belote
import gui
import network

def rotate(value, offset):
	if offset < 0:
		offset += 4
	if isinstance(value, list):
		return value[offset:] + value[:offset]
	elif isinstance(value, int):
		return (value + 4 - offset) % 4

def gui_set(ui, ctx, local):
	def idle_waitroom():
		ui.waitroom(ctx.game_info())

	def idle_playroom():
		table = rotate(local.cards, local.player)
		bonuses = rotate(local.bonuses, local.player)
		ui.playroom(table, local.hand, bonuses, rotate(local.play_hand, local.player))

	remote.notify_position = ui.asynchronous(remote.notify_position, idle_waitroom)
	remote.notify_restart = ui.asynchronous(remote.notify_restart, idle_playroom)
	remote.notify_deal = ui.asynchronous(remote.notify_deal, idle_playroom)
	remote.notify_bid = ui.asynchronous(remote.notify_bid, idle_playroom)
	remote.notify_play = ui.asynchronous(remote.notify_play, idle_playroom)
	remote.notify_score = ui.asynchronous(remote.notify_score, idle_playroom)
	remote.ask_join = ui.asynchronous(remote.ask_join, idle_waitroom)
	remote.ask_bid = ui.asynchronous(remote.ask_bid, idle_playroom)
	remote.ask_play = ui.asynchronous(remote.ask_play, idle_playroom)

class local():
	def __init__(self, name, gui):
		self.name = name
		self.gui = gui
		self.play_first = 0
		self.notify_restart(False)
		self.available = True

		self.player = None
		self.info = None

	role = "local"

	def notify_restart(self, increment=True):
		self.cards = [[], [], [], []]

		self.bid = (None, 0)
		self.double = None
		self.redouble = None

		self.hand = [None] * 4 # stored already rotated
		self.bonuses = [()] * 4 # stored already rotated
		self.hand_cards = 0

		if increment:
			self.play_first += 1
		self.play_hand = self.play_first

	def _playroom(self, bid_player=-1, bid=belote.BID_PASS):
		table = rotate(self.cards, self.player)
		self.gui.playroom(table, self.hand, self.bonuses, rotate(self.play_hand, self.player))
		if self.bid:
			self.gui.display_info(rotate(self.info, self.player), self.player, *self.bid)
		if bid_player >= 0:
			self.gui.display_bid(rotate(bid_player, self.player), bid)

	def _bid_valid(self, new):
		def allies(a, b):
			return (a % 2) == (b % 2)

		if new > 0:
			return new > self.bid[1]
		elif new < 0: # doubling
			if self.bid[0] == None:
				return False
			if self.redouble:
				return False
			elif self.double:
				return (new == belote.BID_REDOUBLE) and allies(self.player, self.bid[0])
			else:
				return (new == belote.BID_DOUBLE) and not allies(self.player, self.bid[0])
		else: # pass
			return True

	def notify_deal(self, player, cards):
		assert player in [0, 1, 2, 3]
		if player == self.player:
			trumps = belote.TRUMPS[self.bid[1]] if self.bid[1] else []
			self.cards[player] = belote.arrange(self.cards[player] + cards, trumps)
		else:
			self.cards[player] += cards

	def notify_bid(self, player, bid):
		assert bid in belote.BIDS
		if bid > 0:
			self.bid = (player, bid)
			self.double = None
			self.redouble = None
		elif bid == belote.BID_DOUBLE:
			self.double = player
		elif bid == belote.BID_REDOUBLE:
			self.redouble = player

		if player != self.player:
			self._playroom(player, bid)
			self.gui.acknowledge()

	def notify_play(self, player, card, bonuses):
		if player == self.player:
			self.cards[player] = [c for c in self.cards[player] if c != card]		
		else:
			self.cards[player].pop()

		index = (player + 4 - self.player) % 4
		self.hand[index] = card
		self.bonuses[index] = bonuses
		self.hand_cards += 1

		if self.hand_cards == 4: # it is time to collect the hand
			first = (player + 1) % 4
			hand = rotate(self.hand, first - self.player)
			self.play_hand = rotate(belote.hand_collect(hand, belote.TRUMPS[self.bid[1]]), -first)

			self._playroom()
			self.gui.acknowledge(1000)

			table = rotate(self.cards, self.player)
			self.gui.hand_animate(table, self.hand, rotate(self.play_hand, self.player))

			self.hand = [None] * 4
			self.bonuses = [()] * 4
			self.hand_cards = 0

	def notify_score(self, names, context, hands, result, points):
		self.gui.score(names, context, hands, result, points)

	def notify_position(self, position, info):
		self.player = position
		self.info = info

	def ask_join(self, index, info):
		self.notify_position(index, info)
		return self.name

	def ask_bid(self):
		self._playroom()
		allowed = {index: gui.BIDS[index] for index in gui.BIDS if self._bid_valid(index)}
		return self.gui.choosebid(allowed, (302, 150))

	def ask_play(self):
		self._playroom()
		playable = cards_allowed(belote.TRUMPS[self.bid[1]], self.hand, self.cards[self.player])
		round = 8 - len(self.cards[self.player])
		return self.gui.select_card(round, playable, self.cards[self.player], belote.TRUMPS[self.bid[1]])

class remote():
	def __init__(self, socket):
		self.name = "<waiting>"
		self.socket_server = socket
		self.play_first = 0
		self.available = False

	role = "remote"

	def notify_restart(self):
		self.play_first += 1
		network.send(self.socket, "notify_restart")

	def notify_deal(self, player, cards):
		assert player in [0, 1, 2, 3]
		network.send(self.socket, "notify_deal", player=player, cards=list(map(str, cards)))

	def notify_bid(self, player, bid):
		assert bid in gui.BIDS
		network.send(self.socket, "notify_bid", player=player, bid=bid)

	def notify_play(self, player, card, bonuses):
		bonuses = [(bonus[0], list(map(str, bonus[1]))) for bonus in bonuses]
		network.send(self.socket, "notify_play", player=player, card=str(card), bonuses=bonuses)

	def notify_score(self, names, context, hands, result, points):
		hands = [list(map(str, hand)) for hand in hands]
		network.send(self.socket, "notify_score", names=names, context=context, hands=hands, result=result, points=points)

	def notify_position(self, position, info):
		self.player = position
		network.send(self.socket, "notify_position", position=self.player, info=info)

	def ask_join(self, index, info):
		self.socket, self.address = network.accept(self.socket_server)
		network.send(self.socket, "ask_join", index=index, info=info)
		self.available = True
		return network.recv(self.socket)["name"]

	def ask_bid(self):
		network.send(self.socket, "ask_bid")
		return network.recv(self.socket)["bid"]

	def ask_play(self):
		network.send(self.socket, "ask_play")
		message = network.recv(self.socket)
		card = belote.card(message["card"])
		bonuses = [(bonus[0], [belote.card(c) for c in bonus[1]]) for bonus in message["bonuses"]]
		return (card, bonuses)

def cards_allowed(trumps, hand, cards):
	def hand_shift(hand):
		"""Shift hand so that cards appear in the order played."""
		first = None
		if hand[0]:
			last = 3 - list(reversed(hand)).index(None)
			first = (last + 1) % 4
		else:
			present = [not not card for card in hand]
			try:
				first = present.index(True)
			except ValueError:
				return None
		return [card for card in (hand[first:] + hand[:first]) if card]

	hand = hand + [None] * (4 - len(hand)) # not using += because it updates in place
	hand = hand_shift(hand)
	if not hand:
		return cards

	suit = hand[0].suit
	hand = [card for card in hand if card]
	cards_suit = [card for card in cards if card.suit == suit]
	if cards_suit: # player has the seeked suit
		if suit in trumps:
			hand_highest = max([card.power(trumps) for card in hand if card.suit == suit])
			cards_higher = [card for card in cards_suit if card.power(trumps) > hand_highest]
			return cards_higher if cards_higher else cards_suit
		else:
			return cards_suit
	elif suit in trumps:
		return cards
	elif not trumps: # player doesn't have the seeked suit; the game is no trumps
		return cards
	else: # the seeked suit is not trump; player doesn't have the seeked suit; the game is a single suit
		if belote.hand_collect(hand, trumps) == (len(hand) - 2): # teammate is holding the hand
			return cards

		hand_trumps = [card.power(trumps) for card in hand if card.suit in trumps]
		cards_trumps = None
		if hand_trumps:
			cards_trumps = [card for card in cards if (card.suit in trumps) and card.power(trumps) > max(hand_trumps)]
		else:
			cards_trumps = [card for card in cards if card.suit in trumps]
		return cards_trumps if cards_trumps else cards
