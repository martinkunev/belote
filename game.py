#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

import gui
import player
import computer
import belote
import score
import network

PLAYERS_TOTAL = 4

def deck_player(deck, player):
	return deck[(player * 8):((player * 8) + 8)]

class context():
	def __init__(self):
		self.players = []

	def game_join(self, player):
		self.players.append(player)

	def game_full(self):
		return len(self.players) == PLAYERS_TOTAL

	def game_info(self):
		return [{"name": p.name, "role": p.role} for index, p in enumerate(self.players)]

	def notification_order(self):
		def is_local(i):
			return self.players[i].role == "local"
		return sorted(list(range(PLAYERS_TOTAL)), key=is_local)

	def _notify_position(self):
		info = self.game_info()
		for i in self.notification_order():
			if self.players[i].available:
				self.players[i].notify_position(i, info)

	def players_switch(self, i, j):
		self.players[i], self.players[j] = self.players[j], self.players[i]
		self._notify_position()

	def player_rename(self, index, name):
		self.players[index].name = name
		self._notify_position()

start = 0
ctx = context()

ui = gui.gui()
answer, inputs = ui.startup()
if answer == gui.GAME_HOST:
	computers = int(inputs["computers"])
	if computers not in [0, 1, 2, 3]:
		print("invalid number of computers")
		exit()

	ctx.game_join(player.local(inputs["name"], ui))
	player.gui_set(ui, ctx, ctx.players[0])

	for i in range(computers):
		ctx.game_join(computer.computer(i))
	if not ctx.game_full():
		socket = network.host()
		while not ctx.game_full():
			ctx.game_join(player.remote(socket))

	for i in range(PLAYERS_TOTAL):
		name = ctx.players[i].ask_join(i, ctx.game_info())
		ctx.player_rename(i, name)

	while True:
		action = ui.waitroom_ready(ctx)
		if not action:
			break
		ctx.players_switch(action[0], action[1])
elif answer == gui.GAME_JOIN:
	network.join(inputs["host"], inputs["name"], ui)
	exit()
else:
	assert answer == gui.GAME_QUIT
	exit()

notify_order = ctx.notification_order()
score = score.score()

players = ctx.players # TODO remove this

while not score.end():
	deck = belote.deal()
	score.new()

	# Deal first 5 cards.
	for index in range(PLAYERS_TOTAL):
		player = (index + start) % PLAYERS_TOTAL

		for i in range(PLAYERS_TOTAL):
			if i == player:
				players[i].notify_deal(player, deck_player(deck, player)[:5])
			else:
				players[i].notify_deal(player, [None] * 5)

	# Ask players for bids.
	index = start
	lastupdate = index
	while True:
		player = index % PLAYERS_TOTAL

		# TODO skip player when the only possible answer is pass. merge this with re-double logic

		# Notification to local player is synchronous.
		# Notify the local player last so that other players don't wait for it.
		bid = players[player].ask_bid()
		notify_order = sorted(list(range(PLAYERS_TOTAL)), key=(lambda i: players[i].role == "local"))
		for i in notify_order:
			players[i].notify_bid(player, bid)
		if bid > 0:
			score.bid = bid
			score.bidder = player
			score.multiplier = 1
			lastupdate = index
		elif bid < 0:
			score.multiplier *= 2
			lastupdate = index
			if bid == belote.BID_REDOUBLE:
				break # no need to carry on

		index += 1
		if (index % PLAYERS_TOTAL) == (lastupdate % PLAYERS_TOTAL):
			break

	if score.bid:
		# Deal last 3 cards.
		for index in range(PLAYERS_TOTAL):
			player = (index + start) % PLAYERS_TOTAL

			for i in notify_order:
				if i == player:
					players[i].notify_deal(player, deck_player(deck, player)[5:])
				else:
					players[i].notify_deal(player, [None] * 3)

		# Ask players to play hands.
		first = start
		for round in range(8):
			# Play a single hand.
			hand = []
			for index in range(PLAYERS_TOTAL):
				player = (index + first) % PLAYERS_TOTAL

				card, bonuses = players[player].ask_play()
				for i in notify_order:
					players[i].notify_play(player, card, bonuses)

				hand.append(card)
				score.bonuses_add(player, bonuses)

			# Give the hand to one team and determine the first player for the next hand.
			first = (first + belote.hand_collect(hand, belote.TRUMPS[score.bid])) % PLAYERS_TOTAL
			score.hand_add(first, hand)

		# Present final score.
		scores = score.calculate()
		""""""
		#print("result: ", own[1], enemy[1])
		#print("points: ", own[0], enemy[0])
		#print("own sequences: ", own[2])
		#print("enemy sequences: ", enemy[2])
		#print("own carres: ", own[3])
		#print("enemy carres: ", enemy[3])

		#print(score.hands[0])
		#print(score.hands[1])
		#print()
		""""""
		names = [player.name for player in players]
		context = {"bid": score.bid, "bidder": score.bidder, "multiplier": score.multiplier, "bonus_last": first % 2}
		for i in notify_order:
			player = (index + start) % PLAYERS_TOTAL
			players[i].notify_score(names, context, score.hands, [scores[0][1], scores[1][1]], [scores[0][0], scores[1][0]])

	start = (start + 1) % PLAYERS_TOTAL

	for player in players:
		player.notify_restart()
