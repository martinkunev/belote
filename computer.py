#! /usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

import random

import belote
import player

class computer(player.local):
	BID_THRESHOLD = {
		belote.BID_CLUBS: 45,
		belote.BID_DIAMONDS: 50,
		belote.BID_HEARTS: 50,
		belote.BID_SPADES: 55,
		belote.BID_NO_TRUMPS: 60,
		belote.BID_ALL_TRUMPS: 60
	}

	_names = ("Bot", "Skynet", "Machine", "AI")

	def __init__(self, name_seed):
		self.name = self._names[name_seed]
		self.play_first = 0
		self.notify_restart(False)
		self.available = True

	role = "computer"

	def notify_restart(self, increment=True):
		self.cards = [[], [], [], []]

		self.bid = (None, 0)
		self.double = None
		self.redouble = None

		self.hand = []

		self.cards_unseen = set(belote.deck())

		if increment:
			self.play_first += 1
		self.play_hand = self.play_first

	def _bid_value(self, bid):
		threshold = None
		if bid < 0:
			if not self.bid[1]:
				return 0
			threshold = int(self.BID_THRESHOLD[self.bid[1]] * 1.2)
			bid = self.bid[1]
		else:
			threshold = self.BID_THRESHOLD[bid]

		score = 0
		trumps = belote.TRUMPS[bid]
		for card in self.cards[self.player]:
			score += card.score(trumps)
		if not trumps:
			score *= 2
		return score if (score >= threshold) else 0

	def ask_join(self, index, info):
		self.player = index
		return self.name

	def notify_deal(self, player, cards):
		assert player in [0, 1, 2, 3]
		self.cards[player] += cards
		if player == self.player:
			for card in cards:
				self.cards_unseen.remove(card)

	def notify_bid(self, player, bid):
		if bid > 0:
			self.bid = (player, bid)
			self.double = None
			self.redouble = None
		elif bid == belote.BID_DOUBLE:
			self.double = player
		elif bid == belote.BID_REDOUBLE:
			self.redouble = player

	def notify_play(self, playing, card, bonuses):
		if playing == self.player:
			self.cards[playing] = [c for c in self.cards[playing] if c != card]
		else:
			self.cards_unseen.remove(card)

		self.hand.append(card)
		if len(self.hand) == 4: # time to collect hand
			first = (playing + 1) % 4
			self.play_hand = (first + belote.hand_collect(self.hand, belote.TRUMPS[self.bid[1]])) % 4

			self.hand = []

	def notify_score(self, names, context, hands, result, points):
		pass

	def notify_position(self, position, info):
		self.player = position

	def ask_bid(self):
		def second(entry):
			return bids[entry]

		bids = {bid: self._bid_value(bid) for bid in belote.BIDS if self._bid_valid(bid) and (bid != belote.BID_PASS)}
		if not bids:
			return belote.BID_PASS
		best = max(bids, key=second)
		return best if bids[best] else belote.BID_PASS

	def ask_play(self):
		trumps = belote.TRUMPS[self.bid[1]]
		playable = player.cards_allowed(trumps, self.hand, self.cards[self.player])

		action_take = None
		action_pile = None # hand going for teammate
		action_discard = None

		player_current = player.rotate(self.player, self.play_hand)

		# TODO support announces

		sequence = self.hand + [None]
		if len(sequence) < 4:
			# Assume any unplayed card could be played by the next players.
			sequence += list(self.cards_unseen)
		for card in playable:
			sequence[len(self.hand)] = card
			collector = belote.hand_collect(sequence, belote.TRUMPS[self.bid[1]])
			if collector == player_current:
				if (not action_take) or (card.power(trumps) < action_take.power(trumps)):
					action_take = card
			elif collector == (player_current - 2):
				# TODO don't pile aces on no trump
				if (not action_pile) or (card.rank > action_pile.rank) or (card.rank == action_pile.rank and (card.suit not in trumps)):
					action_pile = card
			else:
				if (not action_discard) or (card.power(trumps) < action_discard.power(trumps)):
					action_discard = card

		action = action_pile
		if not action_pile:
			action = action_take if action_take else action_discard
		assert action
		return (action, [])
