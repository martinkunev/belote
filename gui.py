#!/usr/bin/python3

"""
Arachni Stream
Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>

This file is part of Belote.

Belote is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Belote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Belote.  If not, see <http://www.gnu.org/licenses/>.
"""

#import sys
#import os
import socket
import time
import threading

from tkinter import *
from PIL import Image, ImageTk

import belote
import score

# TODO stop app when the window is closed (unstopped python processes)

# TODO after announce the cards are not put back

GAME_QUIT = 0
GAME_SINGLE = 1
GAME_HOST = 2
GAME_JOIN = 3

BIDS = {
	belote.BID_REDOUBLE: "re-double",
	belote.BID_DOUBLE: "double",
	belote.BID_PASS: "pass",
	belote.BID_CLUBS: "clubs",
	belote.BID_DIAMONDS: "diamonds",
	belote.BID_HEARTS: "hearts",
	belote.BID_SPADES: "spades",
	belote.BID_NO_TRUMPS: "no donald trump",
	belote.BID_ALL_TRUMPS: "all trump"
}

class gui():
	def __init__(self):
		self.root = Tk()
		width = self.root.winfo_screenwidth()
		height = self.root.winfo_screenheight()
		self.root.geometry("720x720+{}+{}".format((width - 720) // 2, (height - 720) // 2))
		self.root.title("Belote")

		#self.root.protocol("WM_DELETE_WINDOW", self._terminate)

		self.loop = BooleanVar(False)
		self.answer = None

		self.images = {}
		self.inputs = {}
		self.labels = {}
		self.buttons = {}
		self.timer = None

		# managing announces
		self.playable = None
		self.announce = False
		self.selected = set()
		self.bonuses = set()
		self.bonus_pending = None

		self._images_load()

		global interface
		interface = self

	#def _terminate(self):
	#	self.root.destroy()
	#	print("done")
	#	sys.exit("done 1")
	#	exit()
	#	os._exit(0)

	def _images_load(self):
		for card in belote.deck():
			self.images[card] = ImageTk.PhotoImage(image=Image.open("img/" + str(card) + ".png"))

		self.images["side"] = ImageTk.PhotoImage(image=Image.open("img/side.png"))
		self.images["back"] = ImageTk.PhotoImage(image=Image.open("img/back.png"))
		self.images["bonus"] = ImageTk.PhotoImage(image=Image.open("img/bonus.png"))

		self.images["bubble0"] = ImageTk.PhotoImage(image=Image.open("img/bubble_bottom.png"))
		self.images["bubble1"] = ImageTk.PhotoImage(image=Image.open("img/bubble_right.png"))
		self.images["bubble2"] = ImageTk.PhotoImage(image=Image.open("img/bubble_top.png"))
		self.images["bubble3"] = ImageTk.PhotoImage(image=Image.open("img/bubble_left.png"))

	def _cleanup(self):
		for key in self.labels:
			self.labels[key].destroy()
		self.labels = {}

		for key in self.buttons:
			self.buttons[key].destroy()
		self.buttons = {}

	def _handler(self, func, *args):
		return lambda: func(*args)

	def _handler_card(self, func, *args):
		return lambda _: func(*args)

	def _wait(self):
		self.loop.set(True)
		self.root.wait_variable(self.loop)
		self._cleanup()

	def _answer(self, answer):
		self.answer = answer
		self.loop.set(False)

	_position_message = ((342, 542), (522, 342), (342, 142), (162, 342))

	def _position(self, player, width, height):
		x, y = ((342, 442), (422, 342), (342, 242), (262, 342))[player]
		x -= width // 2
		y -= height // 2
		return x, y

	def startup(self):
		self.labels["player"] = Label(self.root, text="Player name")
		self.labels["player"].place(x=20, y=20, width=120, height=20)

		self.inputs["player"] = Entry(self.root)
		self.inputs["player"].place(x=160, y=20, width=120, height=25)
		self.inputs["player"].insert(END, socket.gethostname())

		self.buttons["host"] = Button(self.root, text="Host", command=self._handler(self._answer, GAME_HOST))
		self.buttons["host"].place(x=20, y=60, width=100, height=20)

		self.labels["computers"] = Label(self.root, text="Computer players")
		self.labels["computers"].place(x=60, y=80, width=120, height=20)

		self.inputs["computers"] = Entry(self.root)
		self.inputs["computers"].place(x=200, y=80, width=120, height=25)
		self.inputs["computers"].insert(END, "0")

		self.buttons["join"] = Button(self.root, text="Join", command=self._handler(self._answer, GAME_JOIN))
		self.buttons["join"].place(x=20, y=120, width=100, height=20)

		self.labels["host"] = Label(self.root, text="Host")
		self.labels["host"].place(x=60, y=140, width=120, height=20)

		self.inputs["host"] = Entry(self.root)
		self.inputs["host"].place(x=200, y=140, width=120, height=25)
		self.inputs["host"].insert(END, "127.0.0.1")

		self.buttons["quit"] = Button(self.root, text="Quit", command=self._handler(self._answer, GAME_QUIT))
		self.buttons["quit"].place(x=20, y=180, width=100, height=20)

		self._wait()

		result = {"name": self.inputs["player"].get(), "computers": self.inputs["computers"].get(), "host": self.inputs["host"].get()}
		self.inputs["player"].destroy()
		self.inputs["computers"].destroy()
		self.inputs["host"].destroy()

		return (self.answer, result)

	def waitroom(self, players):
		if not players:
			self.labels["connecting"] = Label(self.root, text="connecting...")
			self.labels["connecting"].place(x=20, y=40, width=150, height=20)
			return

		index = 0
		for player in players:
			info = player["name"] + " (" + player["role"] + ")"
			self.labels["player" + str(index)] = Label(self.root, text=info)
			self.labels["player" + str(index)].place(x=20, y=(40 + index * 20), width=150, height=20)
			index += 1

	def waitroom_ready(self, ctx):
		self.waitroom(ctx.game_info())

		self.buttons["switch01"] = Button(self.root, text="switch", command=self._handler(self._answer, (0, 1)))
		self.buttons["switch01"].place(x=170, y=50, width=60, height=20)

		self.buttons["switch12"] = Button(self.root, text="switch", command=self._handler(self._answer, (1, 2)))
		self.buttons["switch12"].place(x=170, y=70, width=60, height=20)

		self.buttons["switch23"] = Button(self.root, text="switch", command=self._handler(self._answer, (2, 3)))
		self.buttons["switch23"].place(x=170, y=90, width=60, height=20)

		self.buttons["start"] = Button(self.root, text="Start Game", command=self._handler(self._answer, False))
		self.buttons["start"].place(x=20, y=150, width=100, height=25)

		self._wait()
		return self.answer

	def _display_card(self, c, x, y, name=""):
		image = self.images[name] if name else self.images[c]
		self.labels[c] = Label(self.root, image=image)
		self.labels[c].place(x=x, y=y, width=image.width(), height=image.height())

	def _message(self, position, text):
		position_bubble = ((280, 480), (400, 300), (280, 40), (40, 300))[position]
		offset_text = ((10, 10), (10, 10), (10, 80), (70, 10))[position]
		width, height = (140, 70)

		image = self.images["bubble" + str(position)]
		label = Label(self.root, image=image)
		label.place(x=position_bubble[0], y=position_bubble[1], width=image.width(), height=image.height())
		self.labels["bubble" + str(position)] = label

		label = Label(self.root, text=text, bg="#ffffff")
		label.place(x=(position_bubble[0] + offset_text[0]), y=(position_bubble[1] + offset_text[1]), width=width, height=height)
		self.labels["message" + str(position)] = label

	def display_bid(self, position, bid):
		self._message(position, BIDS[bid])

	def choosebid(self, choices, offset=(0, 0)):
		for choice in choices:
			self.buttons[choice] = Button(self.root, text=choices[choice], command=self._handler(self._answer, choice))
			self.buttons[choice].place(x=offset[0], y=(offset[1] + choice * 40), width=100, height=25)
		self._wait()
		return self.answer

	def _select_card(self, card):
		info = self.labels[card].place_info()
		if card in self.selected:
			self.selected.remove(card)
			info["y"] = int(info["y"]) + 25
		else:
			self.selected.add(card)
			info["y"] = int(info["y"]) - 25
		self.labels[card].place(y=info["y"])

	def _select(self, cards, card, trumps):
		# Right click triggers belote.
		if not self.announce:
			print("no announcing")
			if card not in self.playable:
				return
			print("belote test")
			bonus = belote.bonus_belote(cards, card, trumps)
			if bonus:
				self.answer = (card, [bonus])
				self.loop.set(False)
			return

		assert len(self.bonuses) <= 2
		print(self.bonuses)
		if card in set().union({bonus[1] for bonus in self.bonuses}):
			print(card, {bonus[1] for bonus in self.bonuses})
			return # card was already used for a bonus

		self._select_card(card)
		self.bonus_pending = belote.bonus(self.selected)
		self._button_update(self.bonus_pending)
		print(self.bonus_pending)

	def _announce_cancel(self):
		if "announce" in self.buttons:
			self.buttons["announce"].destroy()
			del self.buttons["announce"]

	def _button_update(self, bonus):
		if not bonus:
			self._announce_cancel()
			return

		name = self._bonus_name(bonus)
		if "announce" in self.buttons:
			self.buttons["announce"].config(text=name)
		else:
			self.buttons["announce"] = Button(self.root, text=name, command=self._announce)
			self.buttons["announce"].place(x=615, y=50, width=100, height=25)

	def _announce(self):
		self.selected = set()
		self.bonuses.add(self.bonus_pending)
		self._announce_switch()

	def _announce_switch(self):
		self.announce = not self.announce
		if self.announce:
			self.buttons["announces"].config(text="Cancel")
		else:
			self.buttons["announces"].config(text="Announce")
			self._announce_cancel()
			selected = self.selected.copy()
			for card in selected:
				self._select_card(card)
			self.bonus_pending = None

	def _bonus_name(self, bonus):
		"""Choose display name for the announce."""
		if bonus[0] == score.BONUS_BELOTE:
			return "belote"
		elif bonus[0] == score.BONUS_SEQUENCE:
			return {3: "tierce", 4: "quarte", 5: "quinte"}[len(bonus[1])]
		else:
			assert bonus[0] == score.BONUS_CARRE
			return "carré"

	def display_info(self, info, player, bidder, bid):
		# TODO more info - e.g. who made the bid

		if bid:
			self.root.title("Belote: " + BIDS[bid])

		for i in range(4):
			x, y = ((480, 660), (600, 185), (120, 55), (10, 555))[i]
			self.labels["info"] = Label(self.root, text=info[i]["name"])
			self.labels["info"].place(x=x, y=y, width=120, height=20)

	def hand_animate(self, cards, center, collector):
		self.playroom(cards, center, [], collector)

		delta = ((0, 7), (7, 0), (0, -7), (-7, 0))[collector]
		labels = [self.labels[card] for card in center]
		for step in range(30):
			for index, label in enumerate(labels):
				x, y = self._position(index, 80, 100)
				x += (step + 1) * delta[0]
				y += (step + 1) * delta[1]
				label.place(x=x, y=y)

			self.root.after(50, lambda: self.loop.set(False))
			self.loop.set(True)
			self.root.wait_variable(self.loop)
		self._cleanup()

	def acknowledge(self, delay=2000):
		self.timer = Entry(self.root)
		self.timer.after(delay, self._handler(self._answer, None))
		self._wait()

	def select_card(self, round, playable, cards, trumps):
		if (not round) and trumps:
			self.buttons["announces"] = Button(self.root, text="Announce", command=self._announce_switch)
			self.buttons["announces"].place(x=615, y=25, width=100, height=25)

		if round:
			self.announce = False

		self.playable = playable
		for c in cards:
			if c in playable:
				self.labels[c].bind("<Button-1>", self._handler_card(self._answer, (c, [])))
			self.labels[c].bind("<Button-3>", self._handler_card(self._select, cards, c, trumps))
		self._wait()

		self.answer = (self.answer[0], self.answer[1] + list(self.bonuses))
		self.selected = set()
		self.bonuses = set()
		return self.answer

	def playroom(self, cards, center, bonuses, first):
		for i in range(4):
			if (len(bonuses) > i) and bonuses[i]:
				text = "\n".join([self._bonus_name(bonus) for bonus in bonuses[i]])
				self._message(i, text)
			if center[i]:
				self._display_card(center[i], *self._position(i, 80, 100))

		bottom, right, top, left = tuple(cards)
		for i in range(len(bottom)):
			self._display_card(bottom[i], 220 + (25) * i, 615 - 25 * (bottom[i] in self.selected))
		for i in range(len(right)):
			self._display_card(right[i], 580, 430 - i * 25, "side")
		for i in range(len(top)):
			self._display_card(top[i], 215 + i * 25, 5, "back")
		for i in range(len(left)):
			self._display_card(left[i], 5, 255 + i * 25, "side")

		# Display first player for the current hand.
		position_bonus = ((500, 683), (620, 210), (180, 20), (20, 520))
		x, y = position_bonus[first]
		image = self.images["bonus"]
		self.labels["bonus"] = Label(self.root, image=image)
		self.labels["bonus"].place(x=x, y=y, width=image.width(), height=image.height())

	def score(self, names, context, hands, result, points):
		self._cleanup()
		for widget in self.root.winfo_children():
			widget.pack_forget()
			widget.destroy()

		# TODO display bonuses

		self.root.title("Belote")

		# title

		title = None
		bid = BIDS[context["bid"]]
		if context["multiplier"] == 4:
			title = bid + " (re-double)"
		elif context["multiplier"] == 2:
			title = bid + " (double)"
		else:
			title = bid + " raised by " + names[context["bidder"]]
		self.labels["winner"] = Label(self.root, text=title)
		self.labels["winner"].place(x=210, y=20, width=300, height=20)

		# team even

		self.labels["player0"] = Label(self.root, text=names[0])
		self.labels["player0"].place(x=20, y=65, width=100, height=20)

		self.labels["player2"] = Label(self.root, text=names[2])
		self.labels["player2"].place(x=150, y=65, width=100, height=20)

		index = 0
		for card in hands[0]:
			self._display_card(card, 5 + index * 20, 100)
			index += 1

		self.labels["result0"] = Label(self.root, text=str(result[0]))
		self.labels["result0"].place(x=20, y=200, width=100, height=20)

		self.labels["points0"] = Label(self.root, text="total: {}".format(points[0]))
		self.labels["points0"].place(x=220, y=200, width=100, height=20)

		# team odd

		self.labels["player1"] = Label(self.root, text=names[1])
		self.labels["player1"].place(x=20, y=385, width=100, height=20)

		self.labels["player3"] = Label(self.root, text=names[3])
		self.labels["player3"].place(x=150, y=385, width=100, height=20)

		index = 0
		for card in hands[1]:
			self._display_card(card, 5 + index * 20, 420)
			index += 1

		self.labels["result1"] = Label(self.root, text=str(result[1]))
		self.labels["result1"].place(x=20, y=520, width=100, height=20)

		self.labels["points1"] = Label(self.root, text="total: {}".format(points[1]))
		self.labels["points1"].place(x=220, y=520, width=100, height=20)

		# last hand bonus

		image = self.images["bonus"]
		self.labels["bonus"] = Label(self.root, image=image)
		self.labels["bonus"].place(x=280, y=(380 if context["bonus_last"] else 60), width=image.width(), height=image.height())

		# close button

		self.buttons["close"] = Button(self.root, text="Close", command=self._handler(self._answer, None))
		self.buttons["close"].place(x=615, y=20, width=100, height=25)

		self._wait()

	def asynchronous(self, function, idle):
		def wrapper(*args):
			running = True
			lock = threading.Lock()

			def rotate(array, first):
				return array[first:] + array[:first]

			def main(*args):
				nonlocal running
				self.answer = function(*args)
				lock.acquire()
				running = False
				lock.release()

			def check_finished():
				lock.acquire()
				done = not running
				lock.release()
				if done:
					self.loop.set(False)
				else:
					self.root.after(200, check_finished)

			thread = threading.Thread(target=main, args=args)
			thread.start()

			idle()

			self.loop.set(True)
			self.root.after(200, check_finished)
			self.root.wait_variable(self.loop)
			self._cleanup()
			thread.join()
			return self.answer
		return wrapper

interface = None
